<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testHomepageNavigationToApplicationPage()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'KDE\'s Applications');
        $this->assertNotNull($linkToKrita = $crawler->selectLink('Krita')->link());

        $crawler = $client->click($linkToKrita);
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Krita');
    }
    
    public function testAllApplications()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        
        $this->assertResponseIsSuccessful();
        $crawler->filter('.app > a')->each(function ($node, $i) use ($client) {
            $this->assertNotNull($link = $node->link());
            $crawler = $client->click($link);
            $this->assertResponseIsSuccessful();
            $this->assertSelectorTextContains('h1', $node->text());
        });
    }
}
