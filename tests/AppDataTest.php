<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Tests;

use App\Model\AppData;
use App\Model\AppDataType;
use PHPUnit\Framework\TestCase;

class AppDataTest extends TestCase
{
    /* Test disabled because ebn is broken 
    public function testEbnKMail()
    {
        $kmailData = [
          "X-KDE-Project" => "kde/pim/kmail",
        ];

        $kmail = new AppData("org.kde.kmail2", $kmailData, AppDataType::Application);

        $this->assertEqual($kmail->getEbnDocCheckingUrl() === "http://ebn.kde.org/apidocs/apidox-kde-4.x/pim-kmail.html");
    }

    public function testEbnDolphin()
    {
        $dolphinData = [
            "X-KDE-Project" => "kde/pim/dolphin",
        ];

        $kmail = new AppData("org.kde.dolphin", $dolphinData, AppDataType::Application);

        $this->assertEqual($kmail->getEbnDocCheckingUrl() === "http://ebn.kde.org/apidocs/apidox-kde-4.x/pim-kmail.html");
    }
     */
}
