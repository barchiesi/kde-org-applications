<?php
/**
 * SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace App\Model;

use Doctrine\Common\Annotations\Annotation\Enum;

/**
 * Enum IconType
 * @package App\Model
 */
abstract class IconType {
    const Stock = 0;
    const Local = 1;
    const None = 2;
}
