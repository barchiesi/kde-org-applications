#!/usr/bin/env bash

# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlschwan.eu>
#
# SPDX-License-Identifier: CC0-1.0

FILENAME="kde-org-applications"

function export_pot_file # First parameter will be the path of the pot file we have to create, includes $FILENAME
{
    potfile=$1
    composer install
    php bin/console translation:update --output-format po --force en
    mv translations/messages+intl-icu.en.po "$potfile"
    python3 bin/cleanpot "$potfile"
}

function import_po_files # First parameter will be a path that will contain several .po files with the format LANG.po
{
    podir=$1
    mkdir -p translations
    for translation in $podir/* ; do
        lang=`basename $translation`
        cp "$translation" "translations/messages.${lang}"
        python3 bin/importpo "translations/messages.${lang}"
    done
}
